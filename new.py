import gym
import numpy as np
import random
import matplotlib.pyplot as plt


env = gym.make('MountainCar-v0')
learningRate=0.2
Q={}
positionArray=np.linspace(-1.2,0.6,num=19)
velocityArray=np.linspace(-0.07,0.07,num=15)
for position in positionArray:
    for velocity in velocityArray:
        for action in range(0, 3):
            Q[((round(position, 1), round(velocity, 2)), action)] = 0

episodeArray=[]
wins=[]


state=env.reset()

count=0
for z in range(1,5000):
 EpisodeReward = 0
 for j in range(0,200):
    Position=round(state[0],1)
    Velocity=round(state[1],2)
    gama=1
    epsilon=random.uniform(0,1)
    finalAction=0
    if epsilon<0.01:
        finalAction=env.action_space.sample()
    else:
        best=-99999999
        for i in range(0,3):
            if Q[(Position,Velocity),i]>best:
                best=Q[(Position,Velocity),i]
                bestAction=i
            finalAction=bestAction
    observation, reward, Isdone, info = env.step(finalAction)
    if (observation[0] == 0.6):
        count=count+1
        print("Episode: {}, No. of times reached Flag : {}, Reward: {}".format(z,count,EpisodeReward))
        EpisodeReward = 0
        state = env.reset()
        continue
    NextPosition = round(observation[0], 1)
    NextVelocity = round(observation[1], 2)
    state = [NextPosition, NextVelocity]
    best = -99999999
    nextbestAction=0
    for i in range(0, 3):
        if Q[(NextPosition, NextVelocity), i] > best:
            best = Q[(NextPosition, NextVelocity), i]
            nextbestAction = i
    y = Q[((NextPosition, NextVelocity), finalAction)]
    x = Q[((Position, Velocity), nextbestAction)]
    Q[((Position, Velocity), finalAction)] = x + (learningRate * (reward + (gama * (y - x))))
    EpisodeReward = reward + EpisodeReward
    episodeArray.append(z)
    wins.append(count)

print("came out of loop")

plt.axis([0,5000,0,5000])
plt.plot(episodeArray,wins)
plt.xlabel('Episode')
plt.ylabel('Wins')
plt.show()