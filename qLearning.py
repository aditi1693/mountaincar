import gym
import numpy as np
import random
import matplotlib.pyplot as plt
import pandas as pd

env = gym.make('MountainCar-v0')

#Initialization

lr = 0.2
gama = 1
goalCount = 0
states = 200
episodes = 5000
rewards = []
wins = []
episodeArray = []

QTable = {}
posArray = np.linspace(-1.2, 0.6, num=19)
velArray = np.linspace(-0.07, 0.07, num=15)

#Dictionary

for position in posArray:
    for velocity in velArray:
        for action in range(0, 3):
            QTable[((round(position, 1), round(velocity, 2)), action)] = 0


#Q-Learning

obs = env.reset()
for episode in range(episodes):
    totalReward = 0
    for s in range(states):
        pos = round(obs[0], 1)
        vel = round(obs[1], 2)
        ep = random.uniform(0, 1)
        a = 0

        if ep < 0.01:
            a = np.random.choice(env.action_space.n)
        else:
            best = -99999999
            bestAction = 0
            for i in range(0, 3):
                if QTable[(pos, vel), i] > best:
                    best = QTable[(pos, vel), i]
                    bestAction = i
                a = bestAction



        obs1, reward, terminate, _ = env.step(a)

        if (obs1[0] == 0.6):
            goalCount += 1
            print("Episode: {}, No. of times reached Flag : {}, Reward: {}".format(episode + 1, goalCount, totalReward))
            totalReward = 0
            obs = env.reset()
            continue

        NextPos = round(obs1[0], 1)
        NextVel = round(obs1[1], 2)
        obs = [NextPos, NextVel]
        best = -99999999
        nextAction = 0
        for i in range(0, 3):
            if QTable[(NextPos, NextVel), i] > best:
                best = QTable[(NextPos, NextVel), i]
                nextAction = i

        QTable[((pos, vel), a)] = QTable[((pos, vel), nextAction)]+ (lr * (reward + (gama * (QTable[((NextPos, NextVel), a)] - QTable[((pos, vel), nextAction)]))))
        totalReward = reward + totalReward
        rewards.append(totalReward)
        episodeArray.append(episode)
        wins.append(goalCount)




#visualisation



plt.axis([0,5000,0,3000])
plt.plot(episodeArray,wins)
plt.xlabel('Episode')
plt.ylabel('Wins')
plt.show()

